package worldanalyzer;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class WordAnalyzerTest {
	WordAnalyzer word;

	@Test
	public void testFirstRepeatedCharacterOneLetterRepeated() {
		word = new WordAnalyzer("aardvark");
		char c = word.firstRepeatedCharacter();
		assertEquals('a', c);
	}

	@Test
	public void testFirstRepeatedCharacterTwoLettersRepeated() {
		word = new WordAnalyzer("roommate");
		char c = word.firstRepeatedCharacter();
		assertEquals('o', c);
	}

	@Test
	public void testFirstRepeatedCharacterNoRepeatedLetters() {
		word = new WordAnalyzer("mate");
		char c = word.firstRepeatedCharacter();
		assertEquals(0, c);
	}

	@Test
	public void testFirstRepeatedCharacterRepeatedLetterNotAdjacent() {
		word = new WordAnalyzer("test");
		char c = word.firstRepeatedCharacter();
		assertEquals(0, c);
	}

	@Test
	public void testFirstMultipleCharacterManyLettersRepeated() {
		word = new WordAnalyzer("missisippi");
		char c = word.firstMultipleCharacter();
		assertEquals('i', c);
	}

	@Test
	public void testFirstMultipleCharacterNoRepeatedLetters() {
		word = new WordAnalyzer("mate");
		char c = word.firstMultipleCharacter();
		assertEquals(0, c);
	}

	@Test
	public void testFirstMultipleCharacterOneLetterRepeated() {
		word = new WordAnalyzer("test");
		char c = word.firstMultipleCharacter();
		assertEquals('t', c);
	}

	@Test
	public void testCountRepeatedCharactersNoRepeatedLetterAtFirst() {
		word = new WordAnalyzer("mississippiii");
		int n = word.countRepeatedCharacters();
		assertEquals(4, n);
	}

	@Test
	public void testCountRepeatedCharactersNoRepeatedLettersAndNoAdjacent() {
		word = new WordAnalyzer("test");
		int n = word.countRepeatedCharacters();
		assertEquals(0, n);
	}

	@Test
	public void testCountRepeatedCharactersRepeatedLetterAtFirst() {
		word = new WordAnalyzer("aabbcdaaaabb");
		int n = word.countRepeatedCharacters();
		assertEquals(4, n);
	}

}
